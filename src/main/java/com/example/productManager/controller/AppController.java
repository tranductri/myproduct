package com.example.productManager.controller;


import com.example.productManager.entities.Product;
import com.example.productManager.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;
import java.util.List;

@Controller
public class AppController {
    @Autowired
    private ProductService service;


    @RequestMapping("/")
    public String viewHomePage(Model model) {
        List<Product> listProducts = service.listAll();
        model.addAttribute("listProducts", listProducts);
        return "index";
    }
    @RequestMapping("/new")
    public String showNewProdductForm(Model model){
        Product product= new Product();
        model.addAttribute("product",product);
        return "new_product";
    }
    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("product") Product product){
         service.save(product);
         return "redirect:/";
    }
    @RequestMapping("/edit/{id}")
    public ModelAndView showEditProductForm(@PathVariable(name = "id")Long id){
    ModelAndView mav = new ModelAndView("edit_product");
    Product  product = service.get(id);
    mav.addObject("product",product);
    return mav;
    }
    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id")Long id){
        service.delete(id);
        return "redirect:/";        
    }
}
